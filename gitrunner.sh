#!/bin/bash

echo "Installing gitlab-runner service"
gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner

echo "Starting gitlab-runner service"
gitlab-runner start

echo "Allow lingering for gitlab-runner user"
loginctl enable-linger 1000
