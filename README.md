# Gitlab runner - shell

A custom container for a shell-based gitlab runner.

> The image is tested for use with **podman** on **Linux** hosts with
> **x86_64** arch

**NOTE: If you are only looking for a gitlab runner, the official gitlab runner
containers are more likely what you want:
https://docs.gitlab.com/runner/install/docker.html**



## What is Gitlab Runner?

GitLab Runner is an application that works with GitLab CI/CD to run jobs in a
pipeline.

See [the Gitlab documentation](https://docs.gitlab.com/runner/).



## How to use this image

The image works by spawning a container running systemd, which has a service
unit that starts the gitlab-runner executable.

This particular container is designed for the following two main purposes:

1. To provide an environment with the minimum software I consider a requirement
   to do software development. I develop all my projects in containers, so if
   you have the dependencies from the `Containerfile` installed, my projects
   will most likely compile on your machine the same they do on my machine and
   in the CI.
2. To work with podman instead of docker.

In a typical CI workflow the gitlab-runner instance in this container will
itself spawn other containers to do compilation/tests etc. In practice this
means that we are nesting virtualizations, therefore we must grant the
gitlab-runner container privileges:

```bash
	podman run --privileged -d --restart always --name gitlab-runner-shell \
		-v "gitlab-runner-shell-config:/etc/gitlab-runner" \
		--label "io.containers.autoupdate=registry" \
		registry.gitlab.com/c8160/gitlab-runner-shell:15.0.0
```

Note that the project contains a `Makefile`, so instead of typing the command
above everytime you want to spawn the container, you can simply call `make
run`.



## Starting the container on system boot

To have the container run whenever the system it is running on is restarted, it
is ideally converted to a systemd service unit. This is best achieved with the
Makefile contained in this repo:

```bash
$ make systemd
```

