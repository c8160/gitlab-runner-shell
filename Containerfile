# ===== SOURCES =====
# [1]: https://www.redhat.com/sysadmin/podman-inside-container
# [2]: https://developers.redhat.com/blog/2019/04/24/how-to-run-systemd-in-a-container
# [3]: https://docs.gitlab.com/runner/install/linux-manually.html
# ===================
FROM registry.fedoraproject.org/fedora:36

# Install the required packages to run podman [1]
RUN dnf -y update; dnf -y reinstall shadow-utils; \
    dnf -y install systemd buildah podman git make podman-docker hostname findutils --exclude container-selinux; \
    rm -rf /var/cache /var/log/dnf* /var/log/yum.*

# Create the user for the gitlab-runner binary [3]
# Add content to subuid and subgid for rootless podman [1]
RUN useradd --create-home --shell /bin/bash gitlab-runner; \
    echo gitlab-runner:100000:65536 > /etc/subuid; \
    echo gitlab-runner:100000:65536 > /etc/subgid;

# Get the current gitlab runner binary. We defer actual setup and installation
# to later, see below.
RUN curl -Lo /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64"; \
    chmod +x /usr/local/bin/gitlab-runner
# Custom systemd service that installs gitlab-runner after boot. We need this
# because to register and start the gitlab-runner service, systemd must be
# available. If 'gitlab-runner' doesn't find an online systemd, it will assume
# that Init is used and install **the wrong service file**. Thus we postpone
# the actual installation of the runner to some time after the system has
# started.
ADD install-gitlab-runner.service /etc/systemd/system/install-gitlab-runner.service
# Installs the gitlab-runner [3]
ADD gitrunner.sh /usr/local/bin/gitrunner

# Enable the service that installs gitlab-runner
RUN chown gitlab-runner:gitlab-runner -R /home/gitlab-runner; \
    systemctl enable install-gitlab-runner.service

# Start systemd, see [2]
ENTRYPOINT [ "/sbin/init" ]
