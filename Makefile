CONT_NAME=gitlab-runner-shell
SYSTEMD_UNIT_DIR=/etc/systemd/system

build:
	podman build -t localhost/gitlab-runner-shell:latest .

run: stop
	podman run --privileged -d --restart unless-stopped --name $(CONT_NAME) \
		-v "$(CONT_NAME)-config:/etc/gitlab-runner" \
		--label "io.containers.autoupdate=registry" \
		registry.gitlab.com/c8160/gitlab-runner-shell:15

test: stop
	podman run --privileged -d --restart unless-stopped --name $(CONT_NAME) \
		-v "$(CONT_NAME)-config:/etc/gitlab-runner" \
		--label "io.containers.autoupdate=registry" \
		localhost/gitlab-runner-shell:latest

stop:
	podman stop $(CONT_NAME) 2>/dev/null || true
	podman rm $(CONT_NAME) 2>/dev/null || true

stop_systemd:
	systemctl stop container-$(CONT_NAME).service

systemd: run stop_systemd
	podman generate systemd --name $(CONT_NAME) --new --files
	mv container-$(CONT_NAME).service $(SYSTEMD_UNIT_DIR)
	restorecon $(SYSTEMD_UNIT_DIR)/container-$(CONT_NAME).service
	systemctl daemon-reload
	systemctl enable --now container-$(CONT_NAME).service

enter:
	podman exec -it $(CONT_NAME) bash

# TODO: For rootless
#podman run --security-opt label=disable --device /dev/fuse --rm -it podman-gitrunner

#podman run --name "gitrun" --privileged --rm -d -v "gitlab-runner-config:/etc/gitlab-runner" -v "./container-storage:/home/gitlab-runner/.local/share/containers/storage:Z" podman-gitrunner
#podman run --name "gitrun" --security-opt label=disable --device /dev/fuse --rm -d -v "gitlab-runner-config:/etc/gitlab-runner" -v "./container-storage:/home/gitlab-runner/.local/share/containers/storage:Z" podman-gitrunner
#podman exec -it gitrun bash
#podman kill gitrun

# Problems solved:
# - Making systemd run in the container
# - Registering gitlab-runner as real systemd service
# - Running rootless podman inside rootless podman container
# - Mount volume to podmans container storage to avoid overlayfs in overlayfs (which doesn't work)
#
# TODO:
# - Find out why crun can't create containers (for `RUN`), due to not being able to mount /proc into the container
#
# MAYBE:
# - Download and install gitlab-runner to container in Containerfile, but only install the service later
